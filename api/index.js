const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');

const artists = require('./app/artists');
const albums = require('./app/albums');
const tracks = require('./app/tracks');

const port = 8000;
const app = express();
app.use(express.json());
app.use(cors());
app.use(express.static('public'));

app.use('/artists', artists);
app.use('/albums', albums);
app.use('/tracks', tracks);

const run  = async () => {
    await mongoose.connect('mongodb://localhost/last_fm');

    app.listen(port, () => {
        console.log(`Server started on ${port} successfully.`);
    });

    exitHook(() => {
       console.log('exiting');
       mongoose.disconnect();
    });
};

run().catch(e => {console.log(e)});
