const express = require('express');
const router = express.Router();
const Artist = require('../models/Artist');
const multer = require('multer');
const {nanoid} = require('nanoid');
const config = require('../config');
const path = require('path');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
    try {
        const artists = await Artist.find();
        res.send(artists);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', upload.single('image'), async (req, res) => {
    if (!req.body.name) {
         return res.status(400).send({error: 'Data not valid'});
    }

    const artistData = {
        name: req.body.name,
        info: req.body.info || null
    };

    if (req.file) {
        artistData.image = req.file.image;
    }

    const artist = new Artist(artistData);

    try {
        await artist.save();
        res.send(artist);
    } catch (e) {
        res.status(400).send({error: 'Something went wrong'});
    }
});


module.exports = router;